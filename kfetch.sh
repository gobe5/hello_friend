#!/usr/bin/env bash

# COLORS

c00=$'\e[0;30m'
c01=$'\e[0;31m'
c02=$'\e[0;32m'
c03=$'\e[0;33m'
c04=$'\e[0;34m'
c05=$'\e[0;35m'
c06=$'\e[0;36m'
c07=$'\e[0;37m'
c08=$'\e[1;30m'
c09=$'\e[1;31m'
c10=$'\e[1;32m'
c11=$'\e[1;33m'
c12=$'\e[1;34m'
c13=$'\e[1;35m'
c14=$'\e[1;36m'
c15=$'\e[1;37m'

f0=$'\e[1;30m'
f1=$'\e[1;37m'

# INFO

OS=$(sed '2,$d;s/NAME="//;s/"//' < /etc/os-release)
KERNEL=$(uname -r)
INIT="OpenRC"
UPTIME=$(awk '
  { gsub(/,/,"")
    if (NF==10) print $3;
    else if (NF==11) print $3,$4;
    else if (NF==12) print $3,$4,$5;
    else print $3,$4,$5,$6 }
  ' <(uptime))
WM="I3"
FONT="Sans Mono 11"

cat << EOF
${c00}██${c08}██ ${f1}Distro   : ${c05}${OS}
${c01}██${c09}██ ${f1}Kernel   : ${c01}${KERNEL}
${c03}██${c11}██ ${f1}Uptime   : ${c04}${UPTIME}
${c05}██${c13}██ ${f1}WM       : ${c06}${WM}
${c06}██${c14}██ ${f1}Shell    : ${c12}${SHELL}

EOF